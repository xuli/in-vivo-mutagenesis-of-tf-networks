#!/bin/bash

module load BCFtools

declare -a chr_list=("chr2L" "chr2R")


for chr in "${chr_list[@]}"

do

	#mutagenized samples
	mapfile -t ID_array < bcd_dll_iso_foreground_chr2.sample

	for ID in "${ID_array[@]}"

	do
		printf "%s\n" "${ID}"
		
		#quality filters
		bcftools view -s ${ID} subset/bcd_dll_specific_${chr}.vcf.gz |
			bcftools view -i 'FMT/DP>9 & FMT/DP<201' | 
			bcftools view -i '(FMT/GT="het" & FMT/AO>2 & FMT/RO>2) | FMT/GT!="het"' |
			bcftools view -i 'AC>0' -o sample_by_sample/${ID}_${chr}_filtered.vcf.gz

		bcftools index sample_by_sample/${ID}_${chr}_filtered.vcf.gz

	done


	#control samples
	mapfile -t ID_array < bg_chr2_iso.txt

	for ID in "${ID_array[@]}"

	do
		printf "%s\n" "${ID}"
		bcftools view -s ${ID} subset/bg_NGT40_specific_${chr}.vcf.gz |
			bcftools view -i 'FMT/DP>9 & FMT/DP<201' | 
			bcftools view -i '(FMT/GT="het" & FMT/AO>2 & FMT/RO>2) | FMT/GT!="het"' |
			bcftools view -i 'AC>0' -o sample_by_sample/${ID}_${chr}_filtered.vcf.gz
		bcftools index sample_by_sample/${ID}_${chr}_filtered.vcf.gz

	done

	#merge the variants (horizontally, note that other samples will be set to missing
	bcftools merge sample_by_sample/*${chr}_filtered.vcf.gz -o relaxed_dataset/bcd_dll_specific_filtered_${chr}.vcf.gz

	bcftools index relaxed_dataset/bcd_dll_specific_filtered_${chr}.vcf.gz

	#recover full genotype data from original file
	bcftools isec -c all -p relaxed_dataset/full_sample_${chr} QUAL10_ind/var_AID_ind_${chr}.dm6_multianno.vcf.gz relaxed_dataset/bcd_dll_specific_filtered_${chr}.vcf.gz
	cp relaxed_dataset/full_sample_${chr}/0002.vcf relaxed_dataset/private_${chr}_relaxed.vcf

done

#concat 2L and 2R
bcftools concat relaxed_dataset/private_chr2L_relaxed.vcf relaxed_dataset/private_chr2R_relaxed.vcf -o relaxed_dataset/private_chr2_relaxed.vcf.gz

bcftools norm -m -any relaxed_dataset/private_chr2_relaxed.vcf.gz -o relaxed_dataset/private_chr2_relaxed_norm.vcf.gz

bcftools query -f "%CHROM\t%POS\t%REF\t%ALT\t%QUAL\t%TYPE\t[%GT\t]%Func.refGene\t%Gene.refGene\t%AAChange.refGene\n" relaxed_dataset/private_chr2_relaxed_norm.vcf.gz > relaxed_dataset/private_chr2_relaxed.gt

bcftools query -l relaxed_dataset/private_chr2_relaxed_norm.vcf.gz > relaxed_dataset/chr2.sample




