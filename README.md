# In vivo mutagenesis of gene regulatory networks for the study of the genetics and evolution of the _Drosophila_ regulatory genome

## Description
This GitLab repository provides commands and scripts for the computational analysis in Li et al. (2024). <br/>
An overview of the pipeline is provided in Fig. S3 in the manuscript. 

## Commands for preprocessing the WGS data
### Map reads with `bowtie2`
The sequencing data (available at https://www.ebi.ac.uk/biostudies/arrayexpress/studies/E-MTAB-13796) are in `.txt.gz` format, with file names matching "`*_1_sequence.txt.gz`" or "`*_2_sequence.txt.gz`".<br/>
To precess the data in batch, first generate a txt file with sample IDs.
```
ls *_1_sequence.txt.gz >ID.txt
sed -i s/AAAYL5CHV_// ID.txt #remove prefix strings "AAAYL5CHV_"
sed -i -E s/_23s.+// ID.txt  #remove more prefix strings
```
Include the following commands in a .sbatch file and submit to the cluster:

```
mapfile -t ID_array < ID.txt
ID=${ID_array[$SLURM_ARRAY_TASK_ID]}

bowtie2 -x ~/reference/dm6/dm6 -1 *_${ID}_*_1_sequence.txt.gz -2 *_${ID}_*_2_sequence.txt.gz -N 1 -q -p 4 --no-unal -S sam_files/${ID}.sam

samtools view -bS sam_files/${ID}.sam > bam_files_temp/${ID}.bam

java -Xmx16g -jar $EBROOTPICARD/picard.jar AddOrReplaceReadGroups INPUT=bam_files_temp/${ID}.bam OUTPUT=sorted_bam_temp/${ID}.sorted.bam SORT_ORDER=coordinate RGID=${ID} RGLB=${ID} RGPL=illumina RGPU=${ID} RGSM=${ID}

java -Xmx16g -jar $EBROOTPICARD/picard.jar MarkDuplicates INPUT=sorted_bam_temp/${ID}.sorted.bam OUTPUT=dedup_bam/${ID}.dedup.bam METRICS_FILE=dedup_bam/${ID}.metrics.txt REMOVE_DUPLICATES=true ASSUME_SORTED=true

samtools index dedup_bam/${ID}.dedup.bam
```
### Call variants with `freebayes`
Use `freebayes` to do a joint call across all samples (all dedupped bam files).

Different chromosomal regions can be processed separately to speed up the process. e.g.:

`freebayes -f ~/reference/dm6.fa -r chr3R:20000001-32079331 --min-coverage 10 --min-mapping-quality 30 --min-base-quality 20 --min-alternate-count 2 --genotype-qualities dedup_bam/*.bam >var_AID_freebayes_chr3R_3.vcf`

Concatenate the calls if necessary: 
`bcftools concat var_AID_freebayes_chr*.vcf > var_AID_freebayes_concat.vcf`

## Run pipelines to identify private variants
The shell scripts mentioned below are provided in `/scripts/`. The relevant input files are provided in `/data/`.

### Subset the data to select private variants <br/>
Run `pipeline_QUAL10_and_subset_chrNN.sbatch`:<br/> 
The script takes the joint call (.vcf) as input. <br/>
<br/>
Also it requires <br/>
- A list of all samples relevant to the chromosome of interest (a); <br/>
- A list of mutagenized samples (b); <br/>
- A list of control samples (c). <br/>

(b) and (c) are subset of (a). (a) can include samples besides those in (b) and (c).<br/>

It does the following:
1. Compress and index the input vcf;<br/>
1. Select samples of interest based on the provided sample list;<br/>
1. Select variants of QUAL>10;<br/>
1. Annotate the variants with ANNOVAR;<br/>
1. Compress and index the annotated vcf;<br/>
1. Subset the variants to output <br/>
     (i) variants specific to mutagenized samples;<br/> 
     (ii) variants specific to control samples.<br/>

### Filter chr2 data<br/>

1. Quality filters<br/>
Run `pipeline_relaxed_chr2.sh`, which filters the variants on a sample-by-sample basis: <br/>

- For each sample, filter the private variants generated in the last step to keep those with a sequencing depth between 10 and 200; in the case of a heterozygous call, only keep the variants supported by more than 2 reads for each allele. <br/>
- The filtered variants were merged across samples and re-subsetted from the original joint call, to recover all the genotype data for the selected variants. <br/>
- Mutagenized samples and control samples were treated separately.<br/>

2. Remove samples with weird background<br/>
`bcftools view -S ^../chr2_drop.sample private_chr2_relaxed.vcf.gz -Ou |bcftools view -i 'AC>0' -o private_chr2_relaxed_dropped.vcf.gz`

3. Done for chr2.<br/>

### Filter chr3 data<br/>
1. Check consistency among replicates<br/>
Run `pipeline_check_rep_var.sh`<br/>
Check if the variants are present in more than one replicate regardless of their sequencing depth or allele depth.

2. Quality filters<br/>
Run `pipeline_relaxed_chr3.sh`<br/>
- For each sample, filter the private variants generated in step 1 to keep those with a sequencing depth between 10 and 200; in the case of a heterozygous call, only keep the variants supported by more than 2 reads for each allele. <br/>
- Add variants supported by biological replicates identified in step 2 to the list, regardless of their sequencing depth.
- The filtered variants were merged across samples and re-subsetted from the original joint call, to recover all the genotype data for the selected variants. <br/>
- Mutagenized samples and control samples were treated separately.<br/>

3. Done for chr3.<br/>

### Query the vcf files to generate a genotype table
- Concatenate 2L and 2R, 3L and 3R data if necessary.
- Normalize the alleles for multi-allelic sites, e.g.
`bcftools norm -m -any private_chr2_relaxed_dropped.vcf.gz > private_chr2_relaxed_norm.vcf`
- Query to get genotype and ANNOVAR annotations, e.g.
`bcftools query -f "%CHROM\t%POS\t%REF\t%ALT\t%QUAL\t%TYPE\t[%GT\t]%Func.refGene\t%Gene.refGene\t%AAChange.refGene\n" private_chr3_relaxed.vcf.gz >private_chr3_relaxed.gt`
- The output files (available under `/output/`) were analyzed in R.

## Authors and acknowledgment
The codes are authored by Xueying Li during her postdoctoral work in Justin Crocker's group at EMBL. Please contact lixueying@bnu.edu.cn for any questions.

