#!/bin/bash

module load BCFtools

chr_list=(chr3L chr3R)

#####################
#run pipeline_check_rep_var.sh before running this script
#####################

for chr in "${chr_list[@]}"

do

	#quality filters on an individual basis
	mapfile -t ID_array < bcd_dll_iso_foreground.sample



	for ID in "${ID_array[@]}"

	do
		printf "%s\n" "${ID}"
		bcftools view -s ${ID} subset/bcd_dll_specific_${chr}.vcf.gz |
			bcftools view -i 'AC>0' | 
			bcftools view -i 'FMT/DP>9 & FMT/DP<201'|
			bcftools view -i '(FMT/GT="het" & FMT/AO>2 & FMT/RO>2) | FMT/GT!="het"' -o sample_by_sample/${ID}_${chr}_filtered.vcf.gz

		bcftools index sample_by_sample/${ID}_${chr}_filtered.vcf.gz
	done


	mapfile -t ID_array < bg_chr3_iso_VK33.txt

	for ID in "${ID_array[@]}"
	do
		printf "%s\n" "${ID}"
		bcftools view -s ${ID} subset/bg_VK33_specific_${chr}.vcf.gz |
			bcftools view -i 'AC>0' |
			bcftools view -i 'FMT/DP>9 & FMT/DP<201'|
			bcftools view -i '(FMT/GT="het" & FMT/AO>2 & FMT/RO>2) | FMT/GT!="het"' -o sample_by_sample/${ID}_${chr}_filtered.vcf.gz

		bcftools index sample_by_sample/${ID}_${chr}_filtered.vcf.gz

	done


	##get qual-filtered vcf, merge them horizontally, including both mut and ctrls
	cp sample_by_sample/*${chr}_filtered.vcf.gz* relaxed_dataset/individual_samples/
	rm relaxed_dataset/individual_samples/Bcd_NM5-1*G10*
	bcftools merge relaxed_dataset/individual_samples/*_${chr}_filtered.vcf.gz -o relaxed_dataset/bcd_dll_specific_filtered_${chr}.vcf.gz
	

	#######the code below is dependent on vcf files generated earlier specifically for samples with replicates!

	##get replicated variants regardless of their DP
	##need to index first
	for file in replicated_samples/mut/${chr}/*replicated.vcf.gz
	do
		bcftools index -f ${file}
	done

	for file in replicated_samples/ctrl/${chr}/*replicated.vcf.gz
	do
		bcftools index -f ${file}
	done
	
	############require dir called chr3R and chr3L in indiviudal_samples
	cp replicated_samples/mut/${chr}/*replicated.vcf.gz* relaxed_dataset/individual_samples/${chr}/
	cp replicated_samples/ctrl/${chr}/*replicated.vcf.gz* relaxed_dataset/individual_samples/${chr}/
	rm relaxed_dataset/individual_samples/${chr}/NMBG10_5-1*
	
	###merge replicated variants across samples, including mut and ctrl samples
	bcftools merge relaxed_dataset/individual_samples/${chr}/*replicated.vcf.gz -o relaxed_dataset/bcd_dll_specific_replicated_${chr}.vcf.gz
	

	####prepare for merging the data, i.e. adding replicated variants to the relaxed dataset regardless of their DP
	
	bcftools index relaxed_dataset/bcd_dll_specific_replicated_${chr}.vcf.gz

	#########requires sample_with_replicates.txt here#######

	#deal with samples with replicates
	bcftools view -S relaxed_dataset/sample_with_replicates.txt --force-samples relaxed_dataset/bcd_dll_specific_filtered_${chr}.vcf.gz -Ou |
		bcftools concat --allow-overlaps -D relaxed_dataset/bcd_dll_specific_replicated_${chr}.vcf.gz -o relaxed_dataset/temp_${chr}_repsamples.vcf.gz


	#deal with samples without replicates
	bcftools view -S ^relaxed_dataset/sample_with_replicates.txt --force-samples relaxed_dataset/bcd_dll_specific_filtered_${chr}.vcf.gz -o relaxed_dataset/temp_${chr}_nonrep.vcf.gz

	bcftools index relaxed_dataset/temp_${chr}_repsamples.vcf.gz
	bcftools index relaxed_dataset/temp_${chr}_nonrep.vcf.gz

	#merge horizontally to combine samples with and without replicates
	bcftools merge relaxed_dataset/temp_${chr}_repsamples.vcf.gz relaxed_dataset/temp_${chr}_nonrep.vcf.gz -o relaxed_dataset/bcd_dll_specific_relaxed_${chr}.vcf.gz
	bcftools index relaxed_dataset/bcd_dll_specific_relaxed_${chr}.vcf.gz

	#recover the full data
	bcftools isec -c all -p relaxed_dataset/full_sample_${chr} QUAL10_ind/var_AID_ind_${chr}.dm6_multianno.vcf.gz relaxed_dataset/bcd_dll_specific_relaxed_${chr}.vcf.gz

	cp relaxed_dataset/full_sample_${chr}/0002.vcf relaxed_dataset/private_${chr}_relaxed.vcf


done
