#!/bin/bash

module load BCFtools

chr_list=(chr3L chr3R)


#before running the script,
#make sure to create all the dir mentioned below



for chr in "${chr_list[@]}"

do


	mapfile -t ID_array < replicated_samples/mut/ID.txt
	for ID in "${ID_array[@]}"

	do
		printf "%s\n" ${ID} 
		bcftools view -S replicated_samples/mut/${ID} subset/bcd_dll_specific_${chr}.vcf.gz |
			bcftools view -i 'N_PASS(GT!="0/0" & GT!=".")>1' -o replicated_samples/mut/${chr}/${ID}_replicated.vcf.gz
		
		bcftools view -i 'MEAN(FMT/DP)>9' replicated_samples/mut/${chr}/${ID}_replicated.vcf.gz -o replicated_samples/mut/${chr}/${ID}_replicated_DP.vcf.gz
		bcftools index replicated_samples/mut/${chr}/${ID}_replicated_DP.vcf.gz

	done


	mapfile -t ID_array < replicated_samples/ctrl/ID.txt


	for ID in "${ID_array[@]}"
	
	do
		bcftools view -S replicated_samples/ctrl/${ID} subset/bg_VK33_specific_${chr}.vcf.gz |
			bcftools view -i 'N_PASS(GT!="0/0" & GT!=".")>1' -o replicated_samples/ctrl/${chr}/${ID}_replicated.vcf.gz
		
		bcftools view -i 'MEAN(FMT/DP)>9' replicated_samples/ctrl/${chr}/${ID}_replicated.vcf.gz -o replicated_samples/ctrl/${chr}/${ID}_replicated_DP.vcf.gz
		
		bcftools index replicated_samples/ctrl/${chr}/${ID}_replicated_DP.vcf.gz
	done

	cp replicated_samples/mut/${chr}/*_DP* stringent_dataset/individual_sample/${chr}/
	cp replicated_samples/ctrl/${chr}/*_DP* stringent_dataset/individual_sample/${chr}/
	rm stringent_dataset/individual_sample/${chr}/NMBG10_5-1_replicated*

	bcftools merge stringent_dataset/individual_sample/${chr}/*.vcf.gz -o stringent_dataset/bcd_dll_stringent_${chr}.vcf.gz
	bcftools index stringent_dataset/bcd_dll_stringent_${chr}.vcf.gz
	bcftools isec -c all -p stringent_dataset/full_sample_${chr} QUAL10_ind/var_AID_ind_${chr}.dm6_multianno.vcf.gz stringent_dataset/bcd_dll_stringent_${chr}.vcf.gz
	cp stringent_dataset/full_sample_${chr}/0002.vcf stringent_dataset/private_${chr}_stringent.vcf

done

